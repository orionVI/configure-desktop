#!/bin/bash

#kevi david muñoz moreno
#tel 3508655534
#email david.munozm@protonmail.com

clear

echo " por favor escoge una opcion y pulsa enter para continuar o pulsa Ctrl-C para salir:"
echo " Script generado para configurar equipos linux en UniGermana"
echo " desarrollado por David"
echo " pasante año 2018"
echo "--------------------------------------------------------------------"
echo "presione 1 si quiere cambiar el nombre del sistema sin reiniciar"
echo "presione 2 para congelar a un usuario"
echo "presione 3 para verificar si el usuario esta congelado"
echo "presione 4 para hacer el primer paso para descongelar"
echo "presione 5 para hacer el segundo paso para descongelar"
echo "presione 6 para instalar git"
echo "presione 7 para reiniciar el equipo"
echo "presione 8 para descargar e instalar el freeze"
echo "presione 9 para eliminar los residuos de la instalacion"

echo "ingresa una opcion"
# Read the choice from user and store in variable named choice
read choice

#Use case to match the variable value and do appropriate stuff
case $choice in
1)
echo "Cambiar nombre"
echo "-----------------------"
#Buscar Hostname $hostn
hostn=$(cat /etc/hostname)

#Mostrar hostname existente
echo "Existe este hostname $hostn"

#Preguntar por el nuevo HostName $newhost
echo "Ingresa el nuevo HostName: "
echo "-----------------------"
read newhost

#Cambiar HostName en /etc/hosts & /etc/hostname
sudo sed -i "s/$hostn/$newhost/g" /etc/hosts
sudo sed -i "s/$hostn/$newhost/g" /etc/hostname;;


2)
echo "congelar usuario"
echo "-------------------------"
#A congelar se dijo
echo "Congelare ahora el sistema y lo reiniciare"
echo "Ingresa el nombre del usuario a congelar"
echo "-----------------------"
read USUARIO

#congelando
sudo linfreeze -f $USUARIO

sudo reboot
;;

3)
echo "verificar usuario congelado"
echo "-----------------------------"

#verificar
linfreeze -l;;

4)
echo "primer paso para descongelar"
echo "-----------------------------"

#Descongelar1
sudo linfreeze -u

echo "Paso 1 de la limpieza completado continua con el paso 2"

sudo reboot;;

5)
echo "segundo paso para descongelar"
echo "-----------------------------"

#Descongelar2
sudo linfreeze -c

echo "limpieza hecha satisfactoriamente!!";;

6)
echo "Instalar complemento git"
echo "-----------------------------"

#git
sudo apt-get install git

echo "instalado satisfactoriamente!!";;


7)
echo "-----------------------------------"
echo "reiniciar es una cosa muy sencilla"
echo "es tan facil como escribir reboot"
echo "-----------------------------------"
echo ""
echo "escribe reboot"
echo "-----------------------"

read reboot

sudo $reboot;;

8)
echo "descargar freeze"
echo "-----------------------------"

#linfreeze
sudo git clone https://gitlab.com/orionVI/linfreeze.git
cd linfreeze
sudo cp linfreeze /usr/bin
sudo gzip -9 linfreeze.1
sudo cp linfreeze.1.gz /usr/share/man/man1
sudo chmod +x /usr/bin/linfreeze

echo "instalado satisfactoriamente!!";;

9)
echo "eliminar ficheros residuales"
echo "-----------------------------"

#eliminar residuos
sudo rm -Rf linfreeze

echo "completo";;

*) echo "profavor reinicia el escript y escoge una opcion entre 1 y 6";;
esac
